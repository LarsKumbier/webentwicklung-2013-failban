<?php
require('FirePHPCore/fb.php');

$credentials = array (
	'lars' => 'foobar',
	'sarah' => 'miau');

mysql_connect('localhost:3306', 'failban');
mysql_select_db('failban');


if ($_REQUEST['action'] == login) {
	$clientIp = $_SERVER['REMOTE_ADDR'];
	$sql = "SELECT id, ip, tries, lasttry FROM failban WHERE ip='$clientIp'";
	$result = mysql_query($sql);
	
	// Ip hat noch keinen Login versucht
	if (mysql_num_rows($result) == 0) {
		FB::info("IP unknown yet");
		$loggedIn = validateCredentials($_REQUEST['username'], $_REQUEST['password']);
		// falls fehlgeschlagen
		if (!$loggedIn) {
			$msg = 'Wrong Password';
			banIp($clientIp, 0, true);
		} else {
			$msg = 'You are logged in!';
			unbanIp($clientIp);
		}
	}
	
	// Ip hat schon einen Loginversuch
	else {
		FB::info("IP already known");
		$row = mysql_fetch_assoc($result);
		$tries = $row['tries'];
		// Bantime ist 2^$tries, aber maximal 30min (1800 Sekunden)
		$bantime = strtotime($row['lasttry']) + min(pow(2, $tries), time()+1800);
		FB::info('Current time: '.time());
		FB::info('Bantime:      '.$bantime);
		if (time() < $bantime){
			$msg = "You are logging in too fast!";
		}
		else {
			$loggedIn = validateCredentials($_REQUEST['username'], $_REQUEST['password']);
			if (!$loggedIn) {
				$msg = 'Wrong Password';
				banIp($clientIp, $tries+1, false);
			} else {
				$msg = 'You are logged in!';
				unbanIp($clientIp);
			}
		}
	}
}

$failBans = getFailBans();

function validateCredentials($username, $password) {
	global $credentials;
	
	if (array_key_exists($username, $credentials) &&
	    $credentials[$username] == $password)
	    return true;
	
	return false;
}


function banIp($clientIp, $tries=0, $new=true) {
	if ($new) {
		$sql = "INSERT INTO failban (ip, tries, lasttry) VALUES ";
		$sql .= "('$clientIp', $tries, '".date('Y-m-d H:i:s')."')";
	} else {
		$sql = "UPDATE failban SET tries=$tries,lasttry='".date('Y-m-d H:i:s')."'";
		$sql .= " WHERE ip='$clientIp'";
	}
	FB::info('BANIP: '.$sql);
	$result = mysql_query($sql);
	if (mysql_errno() != 0)
		die("Mysql-Error during insert: ".mysql_error());
}

function unbanIp($clientIp) {
	$sql = "DELETE FROM failban WHERE ip='$clientIp'";
	$result = mysql_query($sql);
	if (mysql_affected_rows() != 1)
		return false;
	return true;
}

function getFailBans() {
	$sql = 'SELECT * FROM failban';
	$result = mysql_query($sql);
	$return = array();
	while ($row = mysql_fetch_assoc($result))
		$return[] = $row;
	return $return;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Loginhack</title>
	</head>
	<body>
		<?php if (!empty($msg)) : ?>
		<p style="color: red"><?=$msg?></p>
		<?php endif; ?>
		<form action="" method="post">
			<input type="text" name="username" placeholder="username">
			<input type="password" name="password" placeholder="password">
			<input type="submit" name="action" value="login">
		</form>
		
		<h2>Status der Failban</h2>
		<table border=1>
		<?php foreach ($failBans as $entry) : ?>
			<tr>
				<?php foreach ($entry as $key => $value) : ?>
				<td><?=$value?></td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
		</table>
	</body>
</html>
